package de.ripes.gameidcreator.util;

import lombok.Getter;
import java.util.HashMap;

public class ColorTranslator {
    @Getter
    private static HashMap<String,String> colorCodes = new HashMap<>(  );
    public static void setup() {
        colorCodes.put( "0", "000000" );
        colorCodes.put( "1", "0000AA" );
        colorCodes.put( "2", "00AA00" );
        colorCodes.put( "3", "00AAAA" );
        colorCodes.put( "4", "AA0000" );
        colorCodes.put( "5", "AA00AA" );
        colorCodes.put( "6", "FFAA00" );
        colorCodes.put( "7", "AAAAAA" );
        colorCodes.put( "8", "555555" );
        colorCodes.put( "9", "5555FF" );
        colorCodes.put( "a", "26B543" );
        colorCodes.put( "b", "55FFFF" );
        colorCodes.put( "c", "FF5555" );
        colorCodes.put( "d", "FF55FF" );
        colorCodes.put( "e", "C1E010" );
        colorCodes.put( "f", "FFFFFF" );
    }
    public static String replaceColors( String text ) {
        int openTags = 0;
        for( String key : colorCodes.keySet() ) {
            for( char c : text.toCharArray() ) {
                if( c == '§' ) {
                    openTags++;
                }
            }
            text = text.replaceAll( "§" + key, "<span style=\" color:#" + colorCodes.get( key ) + "\">" );
        }
        while( openTags != 0 ) {
            text += "</span>";
            openTags--;
        }
        return text;
    }
}
