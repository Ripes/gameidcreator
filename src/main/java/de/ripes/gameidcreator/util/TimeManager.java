package de.ripes.gameidcreator.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeManager {
    static DateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    static DateFormat hourFormat = new SimpleDateFormat("HH");
    public static String getEndeAnzeige( long ende ) {
        Date date = new Date( ende );
        return format.format( date );
    }
    public String getHour() {
        Date date = new Date( System.currentTimeMillis() );
        return hourFormat.format( date );
    }

    public String getRemainingTimeString( long endTime ) {
        String[] end = getRemainingTime( endTime );
        StringBuilder stringBuilder = new StringBuilder();
        int tage = Integer.valueOf( end[ 0 ] );
        int stunden = Integer.valueOf( end[1] );
        int minuten = Integer.valueOf( end[2] );
        int sekunden = Integer.valueOf( end[0] );
        if( tage != 0 ) {
            String append = tage == 1 ? tage + " Tag " : tage + " Tage ";
            stringBuilder.append( append );
        }
        if( stunden != 0 ) {
            String append = stunden == 1 ? stunden + " Stunde " : stunden + " Stunden ";
            stringBuilder.append( append );
        }
        if( minuten != 0 ) {
            String append = minuten == 1 ? minuten + " Minute " : minuten + " Minuten ";
            stringBuilder.append( append );
        }
        if( sekunden != 0 ) {
            String append = sekunden == 1 ? sekunden + " Sekunde" : sekunden + " Sekunden";
            stringBuilder.append( append );
        }
        return stringBuilder.toString();
    }

    private String[] getRemainingTime(long milli){
        long zeit = 0;
        if( System.currentTimeMillis() > milli ) {
            zeit = System.currentTimeMillis() - milli;
        } else {
            zeit = milli - System.currentTimeMillis();
        }
        zeit = zeit / 1000;
        int tage = 0;
        int stunden = 0;
        int minuten = 0;
        int sekunden = 0;
        if(zeit >= 86400){
            tage = (int) zeit / 86400;
        }
        zeit = zeit - tage * 86400;
        if(zeit > 3600){
            stunden = (int)zeit / 3600;
        }
        zeit = zeit - stunden * 3600;
        if(zeit > 60) {
            minuten = ( int ) zeit / 60;
        }
        zeit = zeit - minuten * 60;
        sekunden = (int) zeit;
        String[] al = new String[4];
        al[0] = Integer.toString(tage);
        al[1] = Integer.toString(stunden);
        al[2] = Integer.toString(minuten);
        al[3] = Integer.toString(sekunden);
        return al;
    }
}
