package de.ripes.gameidcreator.manager;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class CreateQueue extends Thread {
    private final Queue<String> queue = new LinkedBlockingQueue<>(  );

    public CreateQueue( ) {
        this.setDaemon( true );
    }

    @Override
    public void run ( ) {
        while( !this.isInterrupted() ) {
            this.blockThread();
            while( !this.queue.isEmpty() ) {
                String gameID = queue.poll();
                GameIDManager.createGameID( gameID );
            }
        }
    }
    public synchronized void add( String gameid ) {
        queue.add( gameid );
        this.notify();
    }
    private synchronized void blockThread() {
        try {
            while( this.queue.isEmpty() ) {
                this.wait();
            }
        } catch ( Exception exc ) { }
    }

}
