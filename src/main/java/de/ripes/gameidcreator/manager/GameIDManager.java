package de.ripes.gameidcreator.manager;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.ripes.gameidcreator.database.DatabaseManager;
import de.ripes.gameidcreator.util.ColorTranslator;
import de.ripes.gameidcreator.util.TimeManager;
import org.bson.Document;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class GameIDManager {
    public static void createGameID( String gameID ) {
        MongoCollection<Document> collection = DatabaseManager.getMongoDatabase().getCollection( "games" );
        FindIterable<Document> cursor = collection.find( Filters.eq( "gameID", gameID ) );

        Document document = cursor.first();
        if( document == null ) {
            System.out.println( "GameID " + gameID + " not found!" );
            return;
        }

        try {
            PrintWriter printWriter = new PrintWriter( "/var/www/gameID/ids/" + gameID, "UTF-8" );

            String gameType = document.getString( "gameType" );

            String[] siteContent = null;

            if( gameType.equals( "bw" ) ) {

                String historyString = "";

                for( String message : ( List<String> ) document.get( "gameHistory", List.class ) ) {
                    historyString += "<code>" + ColorTranslator.replaceColors( message ) + "</code><br>";
                }

                String teamstring = "";
                Document teams = ( Document ) document.get( "teams" );

                for( String teamColor : teams.keySet() ) {
                    List<String> teamAppends = new ArrayList<>(  );
                    teamAppends.add( "<h3>Team " + teamColor + "</h3>" );
                    teamAppends.add( "<table class=\" highlight\">" );
                    teamAppends.add( "<thead><tr><th>Name</th><th>Kills</th><th>Tode</th><th>Betten</th><th>Punkte</th></tr></thead><tbody>" );
                    Document teamPlayers = ( Document ) teams.get( teamColor );
                    for( String playerName : teamPlayers.keySet() ) {
                        Document playerStats = ( Document ) teamPlayers.get( playerName );
                        teamAppends.add( "<tr>" );
                        teamAppends.add( "<td>" + playerName + "</td>" );
                        teamAppends.add( "<td>" + playerStats.getInteger( "kills" ) + "</td>" );
                        teamAppends.add( "<td>" + playerStats.getInteger( "deaths" ) + "</td>" );
                        teamAppends.add( "<td>" + playerStats.getInteger( "beds" ) + "</td>" );
                        teamAppends.add( "<td>" + playerStats.getInteger( "points" ) + "</td>" );
                        teamAppends.add( "</tr>" );
                    }
                    teamAppends.add( "</tbody></table>" );
                    for( String entry : teamAppends ) {
                        teamstring += entry;
                    }
                }


                siteContent = new String[] {
                        "<ul class=\"collapsible\" data-collapsible=\"\">",
                        "<h1>Bedwars - Spielverlauf</h1>",
                        "<li>",
                        "<div class=\"collapsible-header active\"><i class=\"fa fa-info-circle\"></i><b>Informationen</B></div>",
                        "<div class=\"collapsible-body\">",
                        "<table class=\"highlight info\">",
                        "<tr>",
                        "<td><b>GameID</b></td>",
                        "<td>"+ document.getString( "gameID" ) + "</td>",
                        "</tr>",
                        "<tr>",
                        "<td><b>ServerName</b></td>",
                        "<td>"+ document.getString( "serverName" ) + "</td>",
                        "</tr>",
                        "<tr>",
                        "<td><b>Map</b></td>",
                        "<td>" + document.getString( "map" ) + "</td>",
                        "</tr>",
                        "<tr>",
                        "<td><b>Gewinner</b></td>",
                        "<td><b>" + ColorTranslator.replaceColors(document.getString( "winnerTeam" )) + "</b></td>",
                        "</tr>",
                        "<tr>",
                        "<td><b>Spielstart</b></td>",
                        "<td>" + TimeManager.getEndeAnzeige( document.getLong( "gameStart" ) ) + "</td>",
                        "</tr>",
                        "<tr id=\"border\">",
                        "<td><b>Spielstartende</b></td>",
                        "<td>" + TimeManager.getEndeAnzeige( document.getLong( "gameEnd" ) ) + "</td>",
                        "</tr>",
                        "</table>",
                        "</div>",
                        "</li>",
                        "<div class=\"ph\"></div>",
                        "<li>",
                        "<div class=\"collapsible-header active\"><i class=\"fa fa-users\"></i><b>Teams</b></div>",
                        "<div class=\"collapsible-body\">",
                        teamstring,
                        "</div></li>",
                        "<div class=\"ph\"></div>",
                        "<li>",
                        "<div class=\"collapsible-header active \"><i class=\"fa fa-server\"></i><b>Log</b></div>",
                        "<div class=\"collapsible-body\">",
                        "<div class=\"codebox\">",
                        historyString,
                        "</div>",
                        "</div></li></ul></div>",


                };

            } else if( gameType.equals( "otherGame" ) ) {

            }

            for( String line : siteContent ) {
                printWriter.write( line + "\n" );
            }

            printWriter.close();

        } catch ( FileNotFoundException e ) {
            e.printStackTrace( );
        } catch ( UnsupportedEncodingException e ) {
            e.printStackTrace( );
        }

        System.out.println( "GameID found!" );
    }
}
