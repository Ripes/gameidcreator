package de.ripes.gameidcreator.filemanager;

import lombok.Getter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class GameIDFileManager {
    @Getter
    private static HashMap<Long,String> gameIDTimeouts = new HashMap<>(  );
    private static void startTimer() {
        new Thread( ( ) -> {
            new Timer( ).scheduleAtFixedRate( new TimerTask( ) {
                @Override
                public void run ( ) {
                    synchronized ( GameIDFileManager.class ) {
                        Iterator<Long> idIterator = gameIDTimeouts.keySet().iterator();
                        while( idIterator.hasNext() ) {
                            long timeout = idIterator.next();
                            if( System.currentTimeMillis() > timeout ) {
                                String gameID = gameIDTimeouts.get( timeout );
                                idIterator.remove();
                                try {
                                    Runtime.getRuntime( ).exec( new String[]{ "bash", "-c", "rm /var/www/gameID/" + gameID } ).waitFor();
                                } catch ( InterruptedException e ) {
                                    e.printStackTrace( );
                                } catch ( IOException e ) {
                                    e.printStackTrace( );
                                }
                            }
                        }
                    }
                }
            }, 20L, 20L );
        } ).start();
    }
    public static void startUp() {
        try {
            Runtime.getRuntime( ).exec( new String[]{ "bash", "-c", "rm -r /var/www/gameID/ids" } ).waitFor();
            Runtime.getRuntime( ).exec( new String[]{ "bash", "-c", "mkdir /var/www/gameID/ids" } ).waitFor();
        } catch ( InterruptedException e ) {
            e.printStackTrace( );
        } catch ( IOException e ) {
            e.printStackTrace( );
        }
        startTimer();
    }
}
