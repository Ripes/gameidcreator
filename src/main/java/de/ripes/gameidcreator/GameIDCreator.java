package de.ripes.gameidcreator;

import de.ripes.gameidcreator.database.DatabaseManager;
import de.ripes.gameidcreator.filemanager.GameIDFileManager;
import de.ripes.gameidcreator.jetty.WebServer;
import de.ripes.gameidcreator.manager.CreateQueue;
import de.ripes.gameidcreator.util.ColorTranslator;
import lombok.Getter;

public class GameIDCreator {
    @Getter
    private static CreateQueue createQueue;
    public static void main( String[] args ) {
        // Start Webserver
        new Thread( new WebServer() ).start();
        // Create Database Connection
        DatabaseManager.setupDatabase();
        // Start FileManager
        GameIDFileManager.startUp();
        // Create Queue
        createQueue = new CreateQueue();
        new Thread( createQueue ).start();
        ColorTranslator.setup();
    }
}
