package de.ripes.gameidcreator.database;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;

public class DatabaseManager {
    @Getter
    private static MongoDatabase mongoDatabase;
    public static void setupDatabase() {
        MongoClient mongoClient = new MongoClient( "85.190.154.87" );
        mongoDatabase = mongoClient.getDatabase( "games" );
    }
}
