package de.ripes.gameidcreator.jetty;

import org.eclipse.jetty.server.Server;

public class WebServer extends Thread {
    @Override
    public void run ( ) {
        Server server = new Server( 4250 );
        server.setHandler( new WebHandler() );
        try {
            server.start();
        } catch ( Exception e ) {
            e.printStackTrace( );
        }
        try {
            server.join();
        } catch ( InterruptedException e ) {
            e.printStackTrace( );
        }
    }
}
