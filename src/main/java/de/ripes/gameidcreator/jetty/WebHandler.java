package de.ripes.gameidcreator.jetty;

import de.ripes.gameidcreator.GameIDCreator;
import de.ripes.gameidcreator.manager.CreateQueue;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WebHandler extends AbstractHandler {
    @Override
    public void handle ( String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse ) throws IOException, ServletException {
        String gameID = request.getParameter( "gameID" );
        if( gameID != null ) {
            GameIDCreator.getCreateQueue().add( gameID );
            System.out.println( "Requested GameID " + gameID );
        }
        httpServletResponse.setContentType( "text/html;charset=utf-8" );
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);
    }
}
